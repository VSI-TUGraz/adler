import base64
import os


class Globals:
    __tu_logo_dir = os.path.join(os.getcwd(), 'img/tulogo.png')
    __vsi_logo_dir = os.path.join(os.getcwd(), 'img/logo_vsi.gif')

    tu_image_ = base64.b64encode(open(__tu_logo_dir, 'rb').read())
    vsi_image_ = base64.b64encode(open(__vsi_logo_dir, 'rb').read())