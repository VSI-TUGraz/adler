import dash_bootstrap_components as dbc
import dash_html_components as html


class HtmlGlobals:
    @staticmethod
    def load_input_list(index_name):
        """Return item list
        Args:
            :str: index_name

        Returns:
            list: return_div_children
        """
        item_list = [[html.Div(dbc.Button("Save file", id="save_button_" + index_name, n_clicks=0,
                                                          style={"background-color": "#70BAB0", "margin-top": 15,
                                                                 "margin-bottom": 15,
                                                                 "text-align": "center", "width": "100%"}))]]

        return item_list

    @staticmethod
    def collapse_group(item_list, index_name, list_group_children_name):
        """Return div children
           Group object to collapses
        Args:
            :list: item_list
            :str: index_name
            :list: list_group_children_name

        Returns:
            list: return_div_children
        """
        return_div_children = \
            dbc.ListGroupItem(
                list_group_children_name,
                id={
                    'type': 'dynamic-input',
                    'index': index_name
                },
                n_clicks=0, action=True,
                style={
                    "background-color": "#9AC1BC"}
            ),
        return_div_children += dbc.Collapse(
            dbc.Card(dbc.CardBody([item for sublist in item_list for item in sublist])),
            id={
                'type': 'collapse-output',
                'index': index_name
            },
        ),

        return return_div_children
