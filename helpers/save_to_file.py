import json
from utils.utils import Utils
import os
from helpers.load_from_file import define_types_ids


def get_value_from_div(children):
    """Return value from the div
       Parse and return values direct from divs
    Args:
        :list: children

    Returns:
        list: value_list
        dict: value_dict
        list: final_list
    """
    value_list = []
    value_dict = {}
    final_list = []
    for index, child in enumerate(children):
        if child['type'] == 'Div' and 'n_clicks' in child['props'] and "id" not in child['props']:
            break
        if child['props']['children'] is None:
            break
        if index % 2 == 0:
            if len(child['props']['children']) == 0:
                continue
            if child['props']['id']['type'] == 'dynamic-output-div':
                value_list_, value_dict_, final_list_ = get_value_from_div(child['props']['children'])
                final_list.extend(final_list_)
            elif child['props']['children'] is None:
                break
            else:
                name = child['props']['children'][0]['props']['children']['props']['value']
        else:
            if len(children[index - 1]['props']['children']) == 0:
                continue
            c = get_value_from_children_list(child)
            if c is None:
                continue
            type_ = c[0]['props']['children'][1]['props']['children']['props']['value']
            id = c[1]['props']['children'][1]['props']['value']
            id = id if type(id) is list else [id]
            value_dict.update({"name": name, "type": type_, "id": id})
            final_list.append(value_dict.copy())
            value_dict.clear()

    return value_list, value_dict, final_list


def get_value_from_children_list(children):
    """Return children from children list
    Args:
        :list: children
    Returns:
        list: children
    """
    if type(children) is dict:
        if 'props' in children:
            return get_value_from_children_list(children['props'])
        elif 'children' in children:
            return get_value_from_children_list(children['children'])
    if type(children) is list:
        return children


def get_value_from_children(children):
    """Return children
       Recursive function to extract children value
    Args:
        :list: children
    Returns:
        dict:
    """
    if type(children) is dict:
        if 'props' in children:
            return get_value_from_children(children['props'])
        elif 'children' in children:
            return get_value_from_children(children['children'])
    if type(children) is list:
        Utils.new_sub_dict = False
        return get_value_from_div(children)


def parse_inputs_from_div(children):
    """Return dict
       Parse inputs from div to dict
    Args:
        :list: children
    Returns:
        dict: temp_dict
    """
    # output_list = []
    if type(children) is list:
        for child in children:
            temp_dict = {}
            if 'props' in child:
                if 'children' in child['props']:
                    if type(child['props']['children']) is str:
                        key_ = child['props']['children']
                        key_ = "OBJECTS" if key_ == "Object Definition" else key_
                        continue
                    elif type(child['props']['children']) is dict:
                        ch = get_value_from_children(child['props']['children'])
                        if ch is None:
                            continue
                        else:
                            temp_dict.update({key_: ch[2]})
    else:
        print("Children is not a list")
    return temp_dict


def save_data_to_file(args, path_to_file, object_definition=False):
    """Return boolean value
       Save page content to file
    Args:
        :list: args
        :str: path_to_file
        :bool: object_definition
    Returns:
        bool: True/False
    """
    if object_definition:
        return_value = parse_inputs_from_div(args[0]['props']['children'])
        file_name = os.path.join(path_to_file, "output_dynasaur_objects.def")
    else:
        return_value = parse_inputs_from_div(get_value_from_children_list(args[0]['props']['children'][1]))
        file_name = os.path.join(path_to_file, "output_dynasaur.def")
    try:
        with open(file_name, "w") as fd:
            json.dump(return_value, fd)
            define_types_ids(return_value["OBJECTS"])
            # fd.write(str(return_value))
            return True
    except Exception as e:
        print(e)
        return False
