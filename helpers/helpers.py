import dash_html_components as html
from utils.utils import Utils
import time
import random, os
from helpers.load_from_file import get_data_from_json_objects
from constants.html_constants import HtmlGlobals
from lasso.dyna.Binout import Binout
import numpy as np
from dynasaur.data.data_interface import BinoutData
from dynasaur.utils import logger
from dynasaur.utils.constants import DataTypeMapping

def add_new_file_object():
    """Return html div children
       Add new file to current children div
    Args:

    Returns:
        list: html Div
    """
    item_list = []
    el = Utils.add_new_element["OBJECTS"].copy()
    # define_types_ids(el)
    item_list.append(get_data_from_json_objects(el, 0))
    index_name = "object_definition"
    list_group_children_name = "Object Definition"

    item_list += HtmlGlobals.load_input_list(index_name)
    return_div_children = HtmlGlobals.collapse_group(item_list, index_name, list_group_children_name)

    return [html.Div(className="object_content", children=return_div_children, )]


def append_new_element(id_, children):
    """Return html div children
       Add new element to html children
    Args:
        :int: id_
        :dict: children
    Returns:
        children: html div children
    """
    if id_['index'] == "object":
        return_list = get_data_from_json_objects(Utils.master_temp_object, 0, True)
        if children is None:
            return return_list
        else:
            children += return_list
            return children
    else:
        return "Some ERROR occur!"


def generate_id(key):
    """Return random generated id
       Generate random id
    Args:
        :int: key
    Returns:
        int: return_id
    """

    time_stamp = time.time()
    return_id = key + "?" + str(time_stamp % random.randint(1, 5001))

    return return_id


def extract_ids_and_legend_names_from_binout():
    """
       Extract ids and legend names from binout
    Args:

    Returns:

    """
    print("I am reading binout")
    b = Binout(os.path.join(Utils.binout_directory, "binout*"))

    for object in b.read():
        if object not in Utils.binout_content:
            Utils.binout_content.update({object : {}})
        if object == "glstat":
            Utils.binout_content[object] = {"ids": np.array(([0]))}
            continue
        if "ids" in b.read(object):
            Utils.binout_content[object] = {"ids" : b.read(object, "ids")}
            if "legend" in b.read(object):
                Utils.binout_content[object].update({"legend": b.read(object, "legend").split(" ")})
        else:
            for obj in b.read(object):
                if "ids" in b.read(object, obj):
                    Utils.binout_content[object] = {"ids": b.read(object, obj, "ids")[0]}
                    if "legend" in b.read(object, obj):
                        Utils.binout_content[object].update({"legend": b.read(object, obj, "legend").split(" ")})
    print("I am done")


def extract_dropdown_item_from_dict(key):
    return_list = []
    name = DataTypeMapping.dynasaur_identifier_2_binary_identifier(key["type"])
    if "ids" in Utils.binout_content[name]:
        if len(Utils.binout_content[name]["ids"]):
            label = Utils.binout_content[name]["ids"].flatten()
            print(label)
            return_list.append({'label': label, 'value': label})

    return return_list