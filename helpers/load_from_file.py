import base64
import json
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from utils.utils import Utils

from constants.html_constants import HtmlGlobals
from helpers import helpers as h


def init_read_file(contents, filename, date):
    """Return list of html div children
       Read definition file and parse it to html tags
    Args:
        :str: contents
        :str: filename
        :float: date
        :bool: object
    Returns:
        list: html Div
    """
    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)
    if filename.endswith('.def'):
        json_objects = json.loads(decoded.decode('utf8'))
        item_list = []
        if 'OBJECTS' in json_objects:
            item_list.append(get_data_from_json_objects(json_objects["OBJECTS"], 0))
            index_name = "object_definition"
            list_group_children_name = "Object Definition"
        else:
            return [html.Div(dbc.Button("Please select object definition file!", color="danger", className="mr-1"),)], False

        item_list += HtmlGlobals.load_input_list(index_name)

        return_div_children = HtmlGlobals.collapse_group(item_list, index_name, list_group_children_name)

        return [html.Div(className="object_content", children=return_div_children, )], True


def get_data_from_json_objects(json_object, indent, new_element=False):
    """Return list of html div children
       Checkout json input file and parse json object to html tags
    Args:
        :list: json_object
        :int: indent
        :bool: new_element
    Returns:
        list: html Div
    """
    item_list = []
    for key in json_object:
        index_name = h.generate_id(key["name"])
        children_item = dbc.Input(id=key["name"],
                                  value=key["name"], type="text",
                                  style={"background-color": "#9AC1BC",
                                         "border": "0px"})
        item_list.append(html.Div(children=[dbc.ListGroupItem(children=children_item,
                                           id={
                                               'type': 'dynamic-input',
                                               'index': index_name
                                           },
                                           n_clicks=0, action=True, style={"margin-left": indent,
                                                                           "background-color": "#9AC1BC"}),
                  dbc.Button(html.I(className="fas fa-minus-circle"),
                                    color="danger", className="mr-1",
                                    style={'margin': 2},
                                    id={
                                        'type': 'button-single-line',
                                        'index': str(index_name)
                                    })],
                                  id={
                                      'type': 'div-single-line',
                                      'index': str(index_name)
                                  },
                                style={'display': 'flex'}))
        id_type = h.generate_id("id-{}".format("type"))
        children_list = [html.Div([dbc.Input(id=id_type,
                                   value=str("type"), type="text",
                                   style={"background-color": "#DDEFED"}, disabled=True),
                         html.Div(dcc.Dropdown(id={'type': 'dynamic-id-dropdown', 'index': id_type},
                                               options=[{'label': label, 'value': label} for label in
                                                        Utils.dynasaur_types_labes],
                                               value=key["type"],
                                               style={'width': '100%'}),
                                  style={'width': '100%'})], style={"display": "flex"}),
                         html.Div([dbc.Input(id=h.generate_id("id-{}".format("id")),
                                   value="id", type="text",
                                   style={"background-color": "#DDEFED"}, disabled=True),

                         dbc.Input(id=h.generate_id("id-{}".format("id_input")),
                                   value=key["id"], type="text"),
                         # dcc.Dropdown(id=h.generate_id("id-{}".format("id_input")),
                         #              options=[{'label': name, 'value': name} for name in key["id"]],
                         #              # options=helpers.extract_dropdown_item_from_dict(key),
                         #              multi=True, value=[name for name in key["id"]], style={"width": "100%"})
                                   ],  style={"display": "flex"}),
                         ]

        temp_div = html.Div(children=children_list,
                            style={"margin-left": indent},
                            id={
                                  'type': 'div-single-line',
                                  'index': h.generate_id("id-{}".format(key["name"]))
                            })

        item_list += dbc.Collapse(
            dbc.Card(dbc.CardBody(temp_div)),
            id={
                'type': 'collapse-output',
                'index': index_name
            },
        ),

    add_element_list = [html.Div(id={
        'type': 'dynamic-output-div',
        'index': 'object'
    }), html.Div(
        dbc.ListGroupItem(html.Span(["Add new object", html.I(className="fas fa-plus-circle ml-2")]),
                          id={
                              'type': 'dynamic-input-add',
                              'index': 'object'
                          },
                          n_clicks=0, action=True, style={"margin-left": indent,
                                                          "background-color": "#DDEFED"}))]
    if not new_element:
        item_list += add_element_list
    return item_list


def define_types_ids(json_objects):
    """
      Define types and ids in dict structure
    Args:
        :list: json_object
    Returns:

    """
    Utils.defined_types_names.clear()
    Utils.defined_names_ids.clear()
    for key in json_objects:
        if key["type"] not in Utils.defined_types_names:
            Utils.defined_types_names.update({key["type"]: [key["name"]]})
        else:
            Utils.defined_types_names[key["type"]].append(key["name"])

        if key["name"] not in Utils.defined_names_ids:
            Utils.defined_names_ids.update({key["name"]: key["id"]})
        else:
            Utils.defined_names_ids[key["name"]].append(key["id"])
