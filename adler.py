from helpers import helpers as h
from helpers import load_from_file as lf
from helpers import save_to_file as sf
from dash import Dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, MATCH, ClientsideFunction
from utils.utils import Utils
from constants.globals import Globals
import tkinter
from tkinter import filedialog
import time

FONT_AWESOME = "https://use.fontawesome.com/releases/v5.7.2/css/all.css"

app = Dash(external_stylesheets=[dbc.themes.BOOTSTRAP, FONT_AWESOME], suppress_callback_exceptions=True,
           prevent_initial_callbacks=True)
app.title = "ADLER"
app.config.suppress_callback_exceptions = True

CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

modal_warning = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader("WARNING"),
                dbc.ModalBody("Please select object definition file!"),
                dbc.ModalFooter(
                    dbc.Button("Close", id="close_warning", className="ml-auto")
                ),
            ],
            id="modal_warning",
        ),
    ]
)

tab_objects = [html.Div([html.Div(className="inputs_load", children=[
            dcc.Upload(
                    id='upload-data-objects',
                    children=html.Div([
                        'Object definition - Drag and Drop or ',
                        html.A('Select Files')
                    ]),
                    style={
                        'width': '100%',
                        'height': '60px',
                        'lineHeight': '60px',
                        'borderWidth': '1px',
                        'borderStyle': 'dashed',
                        'borderRadius': '5px',
                        'textAlign': 'center',
                        'margin-bottom': "10px"
                    },
                    multiple=False
                ),
            # dbc.Input(id="binout_path", placeholder="Please paste path to binout"),
            # dbc.Button("Please select path to binout", color="secondary", id="binout_path", style={'width': '100%',
            #             'height': '60px'}),
            # html.Br(),
            html.Br(),
            dbc.Button("Load file", id="load_button_object", disabled=True, block=True, n_clicks=0,
                       style={"background-color": "#70BAB0"}),
            dbc.Button("New file", id="new_button_object", disabled=False, block=True, n_clicks=0,
                       style={"background-color": "#70BAB0"})
        ]
        ),
        html.Br(),
        html.Br(),
        html.Div(id="page_content_object"),
        html.Div(
                    [
                        dbc.Nav(
                            [
                                dbc.NavLink(children=[html.Button('Top', className='TopBtn', id='TopBtn', n_clicks=0)],
                                            href="#"),
                            ]
                        ),
                    ]
                ),

        html.Div(id="json_out", children=[dbc.Modal(
            [
                dbc.ModalHeader("Confirmation"),
                dbc.ModalBody("The file has been saved!"),
                dbc.ModalFooter(
                    dbc.Button("Close", id="close_object", n_clicks=0, className="ml-auto")
                ),
            ],
            id="modal_object",
            centered=True
        ),
        ]),
        html.Div(id="test")], style={"margin-top": "20px"})
    ]

layout = html.Div(
    [
        html.Hr(),
        html.Div(
            children=[
                html.Div(children=[
                    html.A(html.Img(src='data:image/gif;base64,{}'.format(Globals.vsi_image_.decode()),
                                    style={'height': '100px', 'margin-bottom': '30px'}),
                           href='https://www.tugraz.at/institute/vsi/home/')],
                    style={'textAlign': 'left', 'width': '30%', 'margin-left': '25px'}),

                html.Div([html.H1(html.Span(
                    "ADLER",
                    id="tooltip-target",
                    style={"cursor": "pointer"},
                    )),
                    dbc.Tooltip(
                            "Applicable Definition File Editor",
                            target="tooltip-target",
                        )],
                         style={"text-align": "center", 'margin-top': '40px',
                                'margin-left': 'auto', 'margin-right': 'auto'}),

                html.Div(children=[html.A(html.Img(src='data:image/png;base64,{}'.format(Globals.tu_image_.decode()),
                                    style={'height': '60px', 'margin-bottom': '30px', 'margin-top': '35px'}),
                           href='https://www.tugraz.at/home/')], style={'textAlign': 'right', 'width': '30%'})],
            style={'display': 'flex'}), #'background-color': '#F6F6F6'
        html.Hr(),
        dbc.Tabs(
            [
                dbc.Tab(label="Object definition", children=tab_objects, id="tab-object_def",
                        tab_style={"margin-left": "5px", "width": "40%"}, label_style={"border-radius": "8px"}),
            ],
            id="tabs",
        ),
    ]
)

app.layout = html.Div([dcc.Location(id="url"), layout])


@app.callback(
    Output("modal_object", "is_open"),
    [Input("save_button_object_definition", "n_clicks"),
     Input("close_object", "n_clicks")],
    [State("page_content_object", "children"),
     State("modal_object", "is_open")],
    prevent_initial_call=True
)
def save_data_to_file_and_preview_confirmation_object(click, close, args, is_open):
    """Return boolean value
       Save current page inputs in to file
    Args:
        :int: click
        :int: close
        :str: args
        :bool: is_open
    Returns:
        bool: True/False
    """
    if Utils.save_button_object < click:
        Utils.save_button_object = click
        root = tkinter.Tk()
        root.withdraw()
        directory = filedialog.askdirectory()
        root.destroy()
        if sf.save_data_to_file(args, directory, True):
            Utils.save_button_pressed = True
            return True
    elif Utils.close_button_save_object < close:
        Utils.close_button_save_object = close
        return False
    else:
        return False


@app.callback(
    Output("load_button_object", "disabled"),
    [Input("upload-data-objects", "contents")],
    [State('upload-data-objects', 'filename'),
     State('upload-data-objects', 'last_modified')]
)
def load_button_state_object(contents, filename, last_modified):
    """Return state of load file button
    Args:
        :binary: contents
        :int: n_clicks
        :string: filename
        :int: last_modified
    Returns:
        boolean: True/False
    """
    if contents is None:
        time.sleep(0.5)
        return True
    else:
        return False


# @app.callback(
#     [Output("binout_path", "color"),
#      Output("binout_path", "children"),],
#     [Input("binout_path", "n_clicks")],
# )
# def select_binout_file(n_click):
#     """Return error or save path to variable
#     Args:
#         :int: n_clicks
#     Returns:
#         string: color
#         string: children
#     """
#     if n_click:
#         root = tkinter.Tk()
#         root.withdraw()
#         Utils.binout_directory = filedialog.askdirectory()
#         root.destroy()
#         if Utils.binout_directory is None:
#             return "danger", "Please select path to binout"
#         else:
#             return "secondary", str( Utils.binout_directory)


@app.callback(
    Output("page_content_object", "children"),
    [
     Input("load_button_object", "n_clicks"),
     Input("new_button_object", "n_clicks")],
     [State("upload-data-objects", "contents"),
      State('upload-data-objects', 'filename'),
      State('upload-data-objects', 'last_modified'),],
    prevent_initial_call=True
)
def load_output_file_and_import_json_data_object(n, n_new, content, file_name, date):
    """Return list of html div children
       Load given file and show the content on page
    Args:
        :int: n
        :int: n_new
        :list: content
        :str: file_name
        :float: date
    Returns:
        list: html div children
    """
    if Utils.load_button_object < n:
        Utils.load_button_object = n
        return_from_file = lf.init_read_file(content, file_name, date)
        if return_from_file[1]:
            return return_from_file[0]
        else:
            return return_from_file[0]
    elif Utils.new_button_object < n_new:
        Utils.new_button_object = n_new
        return h.add_new_file_object()
    elif Utils.save_button_pressed:
        return [html.Div("")]
    else:
        return [html.Div("")]


@app.callback(
    Output({'type': 'dynamic-output-div', 'index': MATCH}, "children"),
    [Input({'type': 'dynamic-input-add', 'index': MATCH}, "n_clicks")],
    [State({'type': 'dynamic-input-add', 'index': MATCH}, "id"),
     State({'type': 'dynamic-output-div', 'index': MATCH}, "children")],
    prevent_initial_call=True
)
def append_new_element(n, id_, children):
    """Return list of html div children
       Append child to children i.e. element to other elements
    Args:
        :int: n
        :int: id_
        :list: children
    Returns:
        list: html div children
    """
    if n:
        return h.append_new_element(id_, children)


app.clientside_callback(
    ClientsideFunction(
        namespace='client_side',
        function_name='delete_item'
    ),
    Output({'type': 'div-single-line', 'index': MATCH}, "children"),
    [Input({'type': 'button-single-line', 'index': MATCH}, "n_clicks")]
)


app.clientside_callback(
    ClientsideFunction(
        namespace='client_side',
        function_name='toggle_collapse'
    ),
    Output({'type': 'collapse-output', 'index': MATCH}, "is_open"),
    [Input({'type': 'dynamic-input', 'index': MATCH}, "n_clicks")],
    [State({'type': 'collapse-output', 'index': MATCH}, "is_open")]
)


app.clientside_callback(
    ClientsideFunction(
        namespace='client_side',
        function_name='change_upload_name'
    ),
    Output("upload-data-objects", "children"),
    [Input("upload-data-objects", "contents")],
    [State('upload-data-objects', 'filename'),
     State('upload-data-objects', 'last_modified')]
)


if __name__ == "__main__":
    app.run_server(debug=False)