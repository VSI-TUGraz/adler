from dynasaur.utils.constants import ObjectConstantsForData, DefinitionConstants, DataTypeMapping


class Utils:
    dynasaur_types_labes = [ObjectConstantsForData.__dict__[name] for name in ObjectConstantsForData.__dict__ if not name.startswith("__")]
    binout_directory = None
    binout_content = {}#{DataTypeMapping.dynasaur_identifier_2_binary_identifier(name) : {} for name in dynasaur_types_labes}
    dynasaur_ids = ["head", "body"]
    defined_types_names = {}
    defined_names_ids = {"head" : [1]}
    load_button_object = 0
    new_button_object = 0
    save_button_object = 0
    close_button_save_object = 0
    save_button_pressed = False
    new_sub_dict = False

    add_new_element = {
        "OBJECTS": [
            {
                "type": "NODE",
                "name": "Name",
                "id": [1]
            }
        ]
    }
    master_temp_object = \
        [{
            "type": "NODE",
            "name": "Name",
            "id": [1]
        }]


