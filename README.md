# ADLER

ADLER - (A Def FiLe Editor)

Definition file are part of [Dynasaur](https://gitlab.com/VSI-TUGraz/Dynasaur) Program. 
This is the first version and ADLER is currently just applicable for [Object](https://gitlab.com/VSI-TUGraz/Dynasaur/-/wikis/Object-Definition-File) definition files.  

# Requirements: 
    - python > 3.6 
    - dash python library
