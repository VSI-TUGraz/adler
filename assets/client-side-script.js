window.dash_clientside = Object.assign({}, window.dash_clientside, {
    client_side: {
        delete_items: function(click) {
        if(click)
        {
            return [[],[], []];
        }
    },
        delete_item: function(click) {
        if(click)
        {
            return [];
        }
    },
        toggle_collapse: function(n, is_open) {
            if(n)
            {
                return !is_open
            }
            return is_open
    },
        change_upload_name: function(content, file_name, date) {
            if(content != null)
            {
                return file_name
            }
    },
        check_validity_input: function(value) {
            return !value;
    }
    }
});